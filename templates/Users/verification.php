    <div class="container">
      <div class="row justify-content-md-center">
        <div class="col-10 card p-3">
          <h5>
            Your Email has been activated, please click <?=$this->Html->link(
                'here',
                ['controller' => 'Users', 'action' => 'login']
            )?> to redirect to Login Page.
          </h5>
        </div>
      </div>
    </div>
